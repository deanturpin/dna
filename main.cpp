#include <algorithm>
#include <array>
#include <execution>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <vector>

using histogram_t = std::array<size_t, 10>;

// Create a histogram from a sequence
histogram_t create_histogram(const std::string file) {

  // Create empty histogram
  histogram_t histogram;
  std::ranges::fill(histogram, 0);

  // Open the file
  std::ifstream in{file};

  // Skip the first comment line
  std::string line;
  std::getline(in, line);

  // Process each line and count occurances of each base
  while (std::getline(in, line))
    for (const char &base : line) {
      switch (base) {
      case 'A':
        ++histogram[0];
        break;
      case 'C':
        ++histogram[1];
        break;
      case 'G':
        ++histogram[2];
        break;
      case 'N':
        ++histogram[3];
        break;
      case 'T':
        ++histogram[4];
        break;
      case 'a':
        ++histogram[5];
        break;
      case 'c':
        ++histogram[6];
        break;
      case 'g':
        ++histogram[7];
        break;
      case 'n':
        ++histogram[8];
        break;
      case 't':
        ++histogram[9];
        break;

      default:
        break;
      }
    }

  return histogram;
}

int main() {

  // Get DNA sequence file names
  const std::vector<std::filesystem::directory_entry> files{
      std::filesystem::directory_iterator("data"), {}};

  // Dump files to process
  std::ranges::copy(
      files,
      std::ostream_iterator<std::filesystem::directory_entry>(std::cout, "\n"));

  // Create empty histograms for each file
  std::vector<histogram_t> histograms(files.size());

  // Calculate histograms
  std::transform(std::execution::par, files.cbegin(), files.cend(),
                 histograms.begin(), [&](const auto &file) {
                   // Read sequence and generate histogram
                   return create_histogram(file.path());
                 });

  // Dump results
  for (const auto &histogram : histograms) {

    // Report count for each base
    const size_t total_bases =
        std::accumulate(histogram.cbegin(), histogram.cend(), 0ul,
                        [](size_t sum, const auto &h) {
                          std::cout << h << "\n";
                          return sum += h;
                        });

    std::cout << "TOTAL\t" << total_bases << "\n";
  }
}
