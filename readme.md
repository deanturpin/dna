# Investigating DNA sequencing in various languages

- https://en.wikipedia.org/wiki/DNA_sequencing

# FASTA file format
> In bioinformatics and biochemistry, the FASTA format is a text-based format
> for representing either nucleotide sequences or amino acid (protein)
> sequences, in which nucleotides or amino acids are represented using
> single-letter codes. The format also allows for sequence names and comments
> to precede the sequences. The format originates from the FASTA software
> package, but has now become a near universal standard in the field of
> bioinformatics.

See [FASTA](https://en.wikipedia.org/wiki/FASTA_format).

# Performance strategies
- Initialising the map with expected bases
- Using a `std::vector` rather than a `std::map`
- Counting lines of all the same character (`std::all_of`)
- Counting sequencess of the same character before pushing to the map
- Skipping the first line to remove the check for a comment in the main loop
- Don't do it in parallel :)
- Don't use `std::stringstream`
- Read file into a string in one go: `const std::string contents{std::istreambuf_iterator<char>(in), {}};`
- Use `std::unordered_map`

# Valgrind comparison
## Sequential
```
==9041==
==9041== I   refs:      22,829,044,186
==9041== I1  misses:             3,547
==9041== LLi misses:             3,334
==9041== I1  miss rate:           0.00%
==9041== LLi miss rate:           0.00%
==9041==
==9041== D   refs:       8,810,170,596  (7,514,175,286 rd   + 1,295,995,310 wr)
==9041== D1  misses:        70,280,634  (   35,149,777 rd   +    35,130,857 wr)
==9041== LLd misses:        68,056,339  (   32,926,761 rd   +    35,129,578 wr)
==9041== D1  miss rate:            0.8% (          0.5%     +           2.7%  )
==9041== LLd miss rate:            0.8% (          0.4%     +           2.7%  )
==9041==
==9041== LL refs:           70,284,181  (   35,153,324 rd   +    35,130,857 wr)
==9041== LL misses:         68,059,673  (   32,930,095 rd   +    35,129,578 wr)
==9041== LL miss rate:             0.2% (          0.1%     +           2.7%  )
```

```
5.58user 0.27system 0:05.88elapsed 99%CPU (0avgtext+0avgdata 273108maxresident)k
0inputs+0outputs (0major+21345minor)pagefaults 0swaps
```

## Parallel
```
==9071==
==9071== I   refs:      22,832,843,030
==9071== I1  misses:             8,474
==9071== LLi misses:             5,102
==9071== I1  miss rate:           0.00%
==9071== LLi miss rate:           0.00%
==9071==
==9071== D   refs:       8,811,664,474  (7,515,133,357 rd   + 1,296,531,117 wr)
==9071== D1  misses:        75,989,855  (   40,715,426 rd   +    35,274,429 wr)
==9071== LLd misses:        69,257,602  (   34,275,420 rd   +    34,982,182 wr)
==9071== D1  miss rate:            0.9% (          0.5%     +           2.7%  )
==9071== LLd miss rate:            0.8% (          0.5%     +           2.7%  )
==9071==
==9071== LL refs:           75,998,329  (   40,723,900 rd   +    35,274,429 wr)
==9071== LL misses:         69,262,704  (   34,280,522 rd   +    34,982,182 wr)
==9071== LL miss rate:             0.2% (          0.1%     +           2.7%  )
```

```
6.82user 0.93system 0:02.20elapsed 352%CPU (0avgtext+0avgdata 943704maxresident)k
0inputs+0outputs (0major+19698minor)pagefaults 0swaps
```

# Examples
- ftp://ftp.ensembl.org/pub/release-102/fasta/homo_sapiens/dna/
- https://www.kaggle.com/ritamenezes/covid19-complete-genomes
- https://www.ncbi.nlm.nih.gov/nuccore/MT019529
- https://www.kaggle.com/fsiamp/dna-sequences?select=1384856121894.data
- https://www.kaggle.com/aashishpawar/promoters
- https://www.kaggle.com/jamzing/sars-coronavirus-accession
- https://www.kaggle.com/neelvasani/chimpanzee-and-dog-dna

---

