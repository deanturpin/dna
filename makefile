objects = $(patsubst %.cpp, tmp/%.o, $(wildcard *.cpp))

all: tmp run
	$(MAKE) -j $(shell nproc) $(objects)

CXX ?= g++-10

apt:
	apt update
	apt install --yes g++-10 libtbb-dev time valgrind cppcheck

CXXFLAGS ?= --std=c++20 --all-warnings --extra-warnings --pedantic-errors \
	-Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy -Wconversion \
	-Og \
	-Wattribute-alias -Wformat-overflow -Wformat-truncation -Wclass-conversion \
	-Wmissing-attributes -Wstringop-truncation

tmp/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ $< -lpthread -ltbb

tmp:
	mkdir -p $@

run: tmp/main.o
	/usr/bin/time $<

clean:
	rm -rf tmp *.bin *.out

format:
	clang-format-10 -i *.cpp

profile: tmp/main.o
	valgrind --tool=cachegrind $<

time: tmp/main.o
	/usr/bin/time --verbose $<

lint:
	cppcheck --enable=all main.cpp

